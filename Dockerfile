FROM adoptopenjdk/openjdk11:ubi
EXPOSE 8080
ARG JAR_FILE=build/libs/ms-securty-0.0.1-SNAPSHOT.jar
ADD ${JAR_FILE} app.jar
RUN apt-get update && apt-get install -y curl
ENTRYPOINT ["java","-jar","/app.jar"]
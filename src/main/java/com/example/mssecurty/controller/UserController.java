package com.example.mssecurty.controller;

import com.example.mssecurty.service.JWTService;
import com.example.mssecurty.dto.UserAuthRequestDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final JWTService jwtService;

    @PostMapping("/auth")
    public String auth(@RequestBody  UserAuthRequestDto request) {
        return jwtService.generateToken(request.getUsername());
    }

    @GetMapping("/getData")
    public String getData() {
        return "Hello get data";
    }

    @GetMapping("/getCredential")
    public String getCredential() {
        return "Hello get crededential";
    }
}

package com.example.mssecurty.service;

import com.example.mssecurty.constants.HttpConstants;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

import java.security.Key;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

@Slf4j
@Configuration
public class JWTService {

    public String generateToken(String ... rule) {
        LocalDate issueDate = LocalDate.now();
        LocalDate expireDate = issueDate.plusDays(1);
        ZoneOffset zoneOffset = ZoneOffset.UTC;
        byte[] signature = HttpConstants.JWT_KEY.getBytes();
        Key key = Keys.hmacShaKeyFor(signature);

        String compact = Jwts.builder()
                .claim("rule", List.of(rule))
                .setSubject("Irshad Eyvazov")
                .setIssuedAt(Date.from(issueDate.atStartOfDay().toInstant(zoneOffset)))
                .setExpiration(Date.from(expireDate.atStartOfDay().toInstant(zoneOffset)))
                .signWith(key)
                .compact();
       log.info("Auth token : {}",compact);
       return compact;

    }




}

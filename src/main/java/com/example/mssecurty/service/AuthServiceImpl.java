package com.example.mssecurty.service;

import com.example.mssecurty.constants.HttpConstants;
import com.example.mssecurty.constants.SecurtyProperty;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final SecurtyProperty property;

    @Override
    public Optional<Authentication> getAuthentication(HttpServletRequest request) {
        Optional<String> authHeader = Optional.ofNullable(request.getHeader(HttpConstants.AUTH_HEADER));
        Optional<String> token = authHeader.filter(this::isBearerValidate).map(this::getToken);
        log.info("Get header token: {}", token);
        return Optional.of(getAuthentication(parseJwt(token.toString())));
    }

    private boolean isBearerValidate(String token) {
        return token.toLowerCase().contains("bearer");
    }

    private String getToken(String jwt) {
        return jwt.split(" ")[1];
    }

    private Authentication getAuthentication(Claims claim) {
        List rules = claim.get("rule", List.class);
        List<GrantedAuthority> grantedAuthorityList = (List<GrantedAuthority>) rules
                .stream()
                .map(m -> new SimpleGrantedAuthority(m.toString()))
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken(claim.getSubject(), "");

    }

    public Claims parseJwt(String token) {
        Claims claim = Jwts.parserBuilder()
                .setSigningKey(property.getJwt().getSecretKey())
                .build()
                .parseClaimsJws(token)
                .getBody();
        log.info("Claim: {}",claim);
        return claim;
    }
}

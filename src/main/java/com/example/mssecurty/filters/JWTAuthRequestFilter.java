package com.example.mssecurty.filters;

import com.example.mssecurty.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
public class JWTAuthRequestFilter extends OncePerRequestFilter {

    private final List<AuthService> authServiceList;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("JWT auth request filter");
        Optional<Authentication> authentication = Optional.empty();
        for(AuthService authService:authServiceList){
            log.trace("Filtering request against auth service {}", authService);

            authentication = authentication.or(() -> authService.getAuthentication(request));
            authentication.ifPresent(auth -> SecurityContextHolder.getContext().setAuthentication(auth));
        }

        filterChain.doFilter(request, response);
    }
}

package com.example.mssecurty.constants;

import lombok.Data;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
@ConfigurationProperties(prefix = "securty")
public class SecurtyProperty {

    private final JwtProperty jwt = new JwtProperty();


    @Data
    public static class JwtProperty{
        private String secretKey;
    }
}

package com.example.mssecurty;

import com.example.mssecurty.service.JWTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsSecurtyApplication implements CommandLineRunner {

	@Autowired
	private JWTService jwtService;

	public static void main(String[] args) {
		SpringApplication.run(MsSecurtyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
    //String token = jwtConfig.generateToken();
    //jwtConfig.parseJwt(token);
	}
}

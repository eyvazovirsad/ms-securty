package com.example.mssecurty.config;

import com.example.mssecurty.service.AuthService;
import com.example.mssecurty.filters.JWTFilterConfigurerAdapter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.List;


@Slf4j
@Configuration
@RequiredArgsConstructor
public class SecurtyConfiguration extends WebSecurityConfigurerAdapter {

    private final List<AuthService> authServiceList;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        log.info("Auth service list size: {} ", authServiceList.size());
        http.apply(new JWTFilterConfigurerAdapter(authServiceList));

        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, "/user/getData").hasAnyAuthority("user")
                .antMatchers(HttpMethod.GET, "/user/getCredential").hasAuthority("admin");

    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers(HttpMethod.POST,"/user/auth");
    }

    @Bean
    @Override
    protected UserDetailsService userDetailsService() {
        UserDetails userDetailUSER = User.withUsername("user")
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
                .password("user").roles("user").build();

        UserDetails userDetailsADMIN = User.withUsername("admin")
                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
                .password("admin").roles("admin").build();

        InMemoryUserDetailsManager inMemoryUserDetailsManager = new InMemoryUserDetailsManager();
        inMemoryUserDetailsManager.createUser(userDetailUSER);
        inMemoryUserDetailsManager.createUser(userDetailsADMIN);
        return inMemoryUserDetailsManager;
    }
}
